export interface IAmqpService {
  connect(): Promise<void>;
  registerQueue(queueName: string, callBack: any): Promise<void>
}
