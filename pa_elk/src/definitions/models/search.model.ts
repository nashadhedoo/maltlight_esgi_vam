import * as mongoose from 'mongoose';
import {Schema, Document} from 'mongoose';
import {ISearch} from '../interfaces';
import {SchemaUtils} from '../../platforms';

export type ISearchDocument = ISearch & Document;

// DeprecationWarning
mongoose.set('useFindAndModify', false);

export const SearchSchema: Schema = new Schema({}, {
    // toObject: SchemaUtils.toPrettyObjectTransform(),
    // toJSON: SchemaUtils.toPrettyObjectTransform(),
    timestamps: true,
});

export const SearchModel = mongoose.model<ISearchDocument>('Search', SearchSchema, 'search');
