import * as Koa from 'koa';
import {getMetadataArgsStorage, RoutingControllersOptions} from 'routing-controllers';
import {validationMetadatasToSchemas} from "class-validator-jsonschema";
import {defaultMetadataStorage} from "class-transformer/storage";
import {routingControllersToSpec} from "routing-controllers-openapi";
import {koaSwagger} from "koa2-swagger-ui";

export const SwaggerService = (app: Koa, routingControllersOptions: RoutingControllersOptions): Koa => {
  const schemas = validationMetadatasToSchemas({
    classTransformerMetadataStorage: defaultMetadataStorage,
    refPointerPrefix: '#/components/schemas/',
  })

  const storage = getMetadataArgsStorage();
  const spec = routingControllersToSpec(storage, routingControllersOptions, {
    components: {
      schemas,
      securitySchemes: {
        basicAuth: {
          scheme: 'basic',
          type: 'http',
        },
      },
    },
    info: {
      description: 'PA_ELK API Documentation',
      title: 'pa_elk docs',
      version: '1.0.0',
    },
  });

  app.use(koaSwagger({
    routePrefix: '/docs',
    swaggerOptions: {spec}
  }));

  return app;
}
