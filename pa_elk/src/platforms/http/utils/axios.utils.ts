import {AxiosResponse} from "axios";
import {ApiError} from "../../../errors";

export class AxiosUtils {

  public static async handle<T>(pendingResponse: Promise<AxiosResponse<T>>): Promise<AxiosResponse<T>> {
    try {
      return await pendingResponse;
    } catch (error: any) {
      if (error.response) {
        const response = error.response as AxiosResponse;
        throw new ApiError(response.status, `Error using http Call : ${response.request.path}    . ${response.data}`);
      }
      throw new Error(`${500} ${error.toString()}`);
    }
  }
}
