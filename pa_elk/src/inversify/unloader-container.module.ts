import {ContainerModule, interfaces} from 'inversify';

export type UnloaderContainerCallBack = (container: interfaces.Container) => Promise<void>;

export class UnloaderContainerModule extends ContainerModule {
  public readonly unload?: UnloaderContainerCallBack;

  constructor(registry: interfaces.ContainerModuleCallBack, unload?: UnloaderContainerCallBack) {
    super(registry);
    this.unload = unload;
  }
}
